# Keycloak Gatekeeper Minimal Setup With Traefik

Partial credit goes to this tutorial with traefik v1: https://github.com/ibuetler/docker-keycloak-traefik-workshop

It still shows how to setup the client and scope in the keycloak admin

## Walkthrough

Setup your /etc/hosts, you need 2 domains that are not localhost.
Here I used keycloak.lan for keycloak auth server, and secret.lan for the service procted by gatekeeper.

Adjust the docker-compose accordingly with these hosts and your LAN/server IP.

Run `docker-compose up` to start everything.

Go to https://keycloak.lan/auth/admin/ and setup the client as described in the old tutorial above. (admin pw is in the docker-compose)

Log out or use private browsing and go to https://keycloak.lan/auth/realms/master/account to register a user account

Edit `gatekeeper/config.yaml` to edit the client-id and client-secret as you've created them.

Restart the gatekeeper. Goto https://secret.lan/dashboard and you should be prompted with a login and then redirected to the traefik-dashboard
(`I'm 881d6fa76105 running on linux/amd64` ...)
